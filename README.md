# README #

Java Currency Convertor (with SpringBoot)

A RESTful API which receives three inputs:

* A source currency
* A target currency
* A monetary value


The API will leverage the exchange rates provided at https://exchangeratesapi.io/ and use that to return the converted value. 
e.g. amount=30 & source_currency=USD & target_currency=GBP

Logic is also exposed by a simple web page
