package se.kurt.rest.converter.currencyconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {UserDetailsServiceAutoConfiguration.class})
@ComponentScan(
    basePackages = {
      "se.kurt.rest.converter.validation",
      "se.kurt.rest.converter.currencyconverter",
      "se.kurt.rest.converter.model",
      "se.kurt.rest.converter.model.properties",
      "se.kurt.rest.converter.providers.exchangeratesio"
    })
@EnableCaching
public class CurrencyConverterApplication {

  public static void main(String[] args) {
    SpringApplication.run(CurrencyConverterApplication.class, args);
  }
}
