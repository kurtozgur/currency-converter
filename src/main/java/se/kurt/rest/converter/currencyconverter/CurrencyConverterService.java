package se.kurt.rest.converter.currencyconverter;

import static se.kurt.rest.converter.MessageConstants.EXCHANGE_RATE_SERVER_ERROR;
import static se.kurt.rest.converter.MessageConstants.EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ;
import static se.kurt.rest.converter.MessageConstants.NO_CONVERSION_RATE_WAS_FOUND_FOR_THE_GIVEN_TARGET_CURRENCY;

import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kurt.rest.converter.exceptions.ConversionFailedException;
import se.kurt.rest.converter.exceptions.UnknownResponseException;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.model.SuccessResponse;
import se.kurt.rest.converter.providers.ExchangeRateProvider;
import se.kurt.rest.converter.providers.exchangeratesio.LatestRateResponseBean;

/** Common class both GUI and REST service can use to provide converted amounts */
public class CurrencyConverterService {

  private static final Logger logger = LoggerFactory.getLogger(CurrencyConverterService.class);

  /**
   * Wrap the calculation result in a SuccesssResponse object for the REST service
   *
   * @param rateProvider
   * @param request
   * @return SuccesssResponse that can be used by the REST service
   * @throws ConversionFailedException
   * @throws UnknownResponseException
   */
  public static SuccessResponse wrapResultInRestResponse(
      ExchangeRateProvider rateProvider, RequestBean request)
      throws ConversionFailedException, UnknownResponseException {
    BigDecimal targetRate = getTargetRateFromProvider(rateProvider, request);

    return new SuccessResponse(
        request, request.getAmountNumeric().multiply(targetRate), targetRate);
  }

  /**
   * Get the rate for conversion from provider service, using the request parameters
   *
   * @param rateProvider backend service that provides exchange rate information
   * @param request request parameters used to query REST service
   * @return A target rate for the requested currencies
   * @throws ConversionFailedException In case of any failure during calculation, with the
   *     appropriate message
   */
  public static BigDecimal getTargetRateFromProvider(
      ExchangeRateProvider rateProvider, RequestBean request)
      throws ConversionFailedException, UnknownResponseException {

    LatestRateResponseBean response =
        (LatestRateResponseBean)
            rateProvider
                .getLatestRate(request)
                .orElseThrow(
                    () -> new UnknownResponseException(EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ));

    if (!response.isSuccess()) {
      logger.error(
          EXCHANGE_RATE_SERVER_ERROR + "\n [Request: {}]\n [Response: {}]", request, response);

      throw new ConversionFailedException(EXCHANGE_RATE_SERVER_ERROR);
    }

    // Get the conversion rate from response
    BigDecimal targetRate = response.getLatestRateFor(request.getTo());

    // Make the conversion for the desired amount
    if (targetRate == null) {
      logger.error(
          NO_CONVERSION_RATE_WAS_FOUND_FOR_THE_GIVEN_TARGET_CURRENCY
              + "\n [Request: {}]\n [Response: {}]\"",
          request,
          response);

      throw new ConversionFailedException(
          NO_CONVERSION_RATE_WAS_FOUND_FOR_THE_GIVEN_TARGET_CURRENCY);
    }

    return targetRate;
  }
}
