package se.kurt.rest.converter.currencyconverter;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.headers()
        // CSRF is on by default on SpringBoot and the token is included in the request by default
        // when thymeleaf is used
        .contentSecurityPolicy(
            contentSecurityPolicy ->
                contentSecurityPolicy.policyDirectives(
                    // unsafe-inline is not ideal
                    "script-src 'self' 'unsafe-inline';"
                        + " object-src 'none';"
                        + " style-src 'self' 'unsafe-inline';"))
        .xssProtection();
  }
}
