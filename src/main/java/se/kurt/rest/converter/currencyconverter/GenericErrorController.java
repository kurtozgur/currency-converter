package se.kurt.rest.converter.currencyconverter;

import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/** Controller for directing unhandled errors to generic error template */
@Controller
public class GenericErrorController implements ErrorController {

  @RequestMapping("/error")
  public String renderErrorPage(HttpServletRequest request, Model model) {
    String errorCode = request.getAttribute("javax.servlet.error.status_code").toString();
    model.addAttribute("errorCode", errorCode);

    return "error";
  }
}
