package se.kurt.rest.converter.currencyconverter;

import static se.kurt.rest.converter.MessageConstants.QUERY_PARAMETERS_ARE_NOT_VALID;

import java.util.StringJoiner;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import se.kurt.rest.converter.exceptions.ConversionFailedException;
import se.kurt.rest.converter.exceptions.UnknownResponseException;
import se.kurt.rest.converter.model.ErrorResponse;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.providers.ExchangeRateProvider;
import se.kurt.rest.converter.validation.LatestRateRequestValidator;

/** Main controller for converter REST service */
@RestController
public class ConverterRestController {

  private final Logger logger = LoggerFactory.getLogger(ConverterRestController.class);

  @Autowired private ExchangeRateProvider rateProvider;
  @Autowired private LatestRateRequestValidator latestRateRequestValidator;

  public ConverterRestController() {}

  @InitBinder
  protected void initBinder(DataBinder binder) {
    binder.addValidators(latestRateRequestValidator);
  }

  @GetMapping(path = "/convert", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> convertCurrency(@Valid RequestBean request) {
    logger.info("Processing a request for the parameters -> {[]}", request);

    try {
      return ResponseEntity.ok()
          .body(CurrencyConverterService.wrapResultInRestResponse(this.rateProvider, request));

    } catch (UnknownResponseException ure) {
      return ResponseEntity.internalServerError().body(new ErrorResponse(ure.getMessage()));

    } catch (ConversionFailedException cfe) {
      return ResponseEntity.internalServerError().body(new ErrorResponse(cfe.getMessage()));

    } catch (Exception e) {
      logger.error(
          "Error while trying to calculate the converted amount, request was: {}", request, e);

      return ResponseEntity.internalServerError()
          .body(new ErrorResponse("Internal error while trying to calculate the converted amount"));
    }
  }

  @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<String> postCurrency() {
    return new ResponseEntity<>("Not supported", HttpStatus.NOT_IMPLEMENTED);
  }

  @PutMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<String> putCurrency() {
    return new ResponseEntity<>("Not supported", HttpStatus.NOT_IMPLEMENTED);
  }

  @DeleteMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<String> deleteCurrency() {
    return new ResponseEntity<>("Not supported", HttpStatus.NOT_IMPLEMENTED);
  }

  // Exception handler for variable validation errors
  @ExceptionHandler({BindException.class})
  private ResponseEntity<ErrorResponse> handleValidationExceptions(
      BindException bindingExceptions) {

    StringJoiner joiner = new StringJoiner(",");

    bindingExceptions
        .getBindingResult()
        .getAllErrors()
        .forEach(
            (error) -> {
              joiner.add(
                  String.format(
                      "[%s]", ((FieldError) error).getField() + ": " + error.getDefaultMessage()));
            });

    String message = QUERY_PARAMETERS_ARE_NOT_VALID + " -> " + joiner.toString();

    return ResponseEntity.badRequest().body(new ErrorResponse(message));
  }
}
