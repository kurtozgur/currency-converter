package se.kurt.rest.converter.currencyconverter;

import java.math.BigDecimal;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StopWatch;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import se.kurt.rest.converter.MessageConstants;
import se.kurt.rest.converter.exceptions.ConversionFailedException;
import se.kurt.rest.converter.exceptions.UnknownResponseException;
import se.kurt.rest.converter.model.AvailableCurrenciesBean;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.providers.ExchangeRateProvider;
import se.kurt.rest.converter.validation.LatestRateRequestValidator;

/** Controller for the web page */
@Controller
public class ConverterWebController {

  @Autowired private ExchangeRateProvider rateProvider;
  @Autowired private LatestRateRequestValidator latestRateRequestValidator;
  @Autowired private AvailableCurrenciesBean availableCurrencies;

  @InitBinder
  protected void initBinder(WebDataBinder binder) {
    binder.addValidators(latestRateRequestValidator);
  }

  @GetMapping("/index")
  public String index(Model model) {
    model.addAttribute(new RequestBean());

    this.setRequestLocale(model);
    model.addAttribute("currencies", this.availableCurrencies.getList());

    return "index";
  }

  @PostMapping("/index")
  public String doConvert(
      @ModelAttribute @Validated RequestBean requestBean,
      BindingResult bindingResult,
      Model model) {

    // To time the process
    StopWatch watch = new StopWatch();
    watch.start();

    if (!bindingResult.hasErrors() && requestBean.getAmount() != null) {
      doConversion(requestBean, model);
    }
    model.addAttribute("currencies", this.availableCurrencies.getList());
    this.setRequestLocale(model);

    watch.stop();

    // set ServerTiming http header
    HttpServletResponse response =
        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    response.setHeader(
        "Server-Timing", String.format("conv;dur=%d;desc=conversion", watch.getTotalTimeMillis()));

    return "index";
  }

  private void doConversion(RequestBean requestBean, Model model) {
    try {
      // Get the rate and make the conversion
      BigDecimal targetRate =
          CurrencyConverterService.getTargetRateFromProvider(this.rateProvider, requestBean);
      model.addAttribute("usedRate", targetRate);
      model.addAttribute("convertedAmount", requestBean.getAmountNumeric().multiply(targetRate));

    } catch (UnknownResponseException | ConversionFailedException e) {
      model.addAttribute("exception", e.getMessage());
    } catch (Exception ex) {
      model.addAttribute("exception", MessageConstants.EXCHANGE_RATE_SERVER_ERROR);
    }
  }

  private void setRequestLocale(Model model) {
    HttpServletRequest request =
        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    Locale clientLocale = new AcceptHeaderLocaleResolver().resolveLocale(request);
    model.addAttribute("locale", clientLocale.toLanguageTag());
  }
}
