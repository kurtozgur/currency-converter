package se.kurt.rest.converter.exceptions;

public class UnknownResponseException extends RuntimeException {

  public UnknownResponseException(String s) {
    super(s);
  }
}
