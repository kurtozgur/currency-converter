package se.kurt.rest.converter.exceptions;

public class ConversionFailedException extends RuntimeException {

  public ConversionFailedException(String s) {
    super(s);
  }
}
