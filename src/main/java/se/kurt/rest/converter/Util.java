package se.kurt.rest.converter;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Util {

  public static final int CURRENCY_DECIMAL_DIGITS = 6;

  public static String bigDecimalToString(BigDecimal input) {
    return input.setScale(CURRENCY_DECIMAL_DIGITS, RoundingMode.HALF_EVEN).toString();
  }
}
