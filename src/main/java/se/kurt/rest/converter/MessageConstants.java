package se.kurt.rest.converter;

public class MessageConstants {

  public static final String NO_CONVERSION_RATE_WAS_FOUND_FOR_THE_GIVEN_TARGET_CURRENCY =
      "No conversion rate was found for the given target currency.";
  public static final String EXCHANGE_RATE_SERVER_ERROR = "Exchange rate server error.";
  public static final String EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ =
      "Exchange rate server response can not be read";

  public static final String ERROR_CODE_FIELD_REQUIRED = "field.required";
  public static final String ERROR_CODE_FIELD_NOT_AVAILABLE = "field.notAvailable";
  public static final String FIELD_NOT_NUMERIC = "field.notNumeric";

  public static final String MSG_FIELD_SHOULD_BE_PROVIDED = "Field should be provided";
  public static final String MSG_THIS_CURRENCY_IS_NOT_AVAILABLE = "This currency is not available";
  public static final String AMOUNT_FIELD_SHOULD_BE_NUMERIC =
      "Amount field should be numeric and greater than zero";

  public static final String QUERY_PARAMETERS_ARE_NOT_VALID = "Query parameters are not valid";

  // Mock server responses
  public static final String CODE_103 = "103";
  public static final String THE_REQUESTED_API_ENDPOINT_DOES_NOT_EXIST =
      "The requested API endpoint does not exist.";

  public static final String CODE_101 = "101";
  public static final String NO_API_KEY_WAS_SPECIFIED_OR_AN_INVALID_API_KEY_WAS_SPECIFIED =
      "No API Key was specified or an invalid API Key was specified.";

  public static final String CODE_106 = "106";
  public static final String THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS =
      "The current request did not return any results.";
}
