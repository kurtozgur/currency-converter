package se.kurt.rest.converter.validation;

import static se.kurt.rest.converter.MessageConstants.AMOUNT_FIELD_SHOULD_BE_NUMERIC;
import static se.kurt.rest.converter.MessageConstants.ERROR_CODE_FIELD_NOT_AVAILABLE;
import static se.kurt.rest.converter.MessageConstants.ERROR_CODE_FIELD_REQUIRED;
import static se.kurt.rest.converter.MessageConstants.FIELD_NOT_NUMERIC;
import static se.kurt.rest.converter.MessageConstants.MSG_FIELD_SHOULD_BE_PROVIDED;
import static se.kurt.rest.converter.MessageConstants.MSG_THIS_CURRENCY_IS_NOT_AVAILABLE;

import java.math.BigDecimal;
import java.util.Set;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import se.kurt.rest.converter.model.AvailableCurrenciesBean;
import se.kurt.rest.converter.model.CurrencySymbol;
import se.kurt.rest.converter.model.RequestBean;

/**
 * Custom input validator for RequestBean
 *
 * <p>Makes one call to the provider service (exchangerates-io) to get the available currencies and
 * the calls to our service will check the availability before sending the request to provider
 */
@Component
public class LatestRateRequestValidator implements Validator {

  private final Set<CurrencySymbol> availableCurrencies;

  @Autowired
  public LatestRateRequestValidator(AvailableCurrenciesBean currencies) {
    // Add a re-trier?
    this.availableCurrencies = currencies.getSet();
  }

  @Override
  public boolean supports(Class<?> classToValidate) {
    return RequestBean.class.isAssignableFrom(classToValidate);
  }

  /**
   * Following validation are made;
   *
   * <p>[from, to, amount] should not be empty
   *
   * <p>[from, to] can only have one of the values from availableCurrencies list
   *
   * <p>[amount] should be convertible to BigDecimal
   */
  @Override
  public void validate(Object target, Errors errors) {

    this.addEmptyFieldValidation(errors);
    // Sanitise inputs beforehand
    this.sanitise((RequestBean) target, errors);

    this.addCurrencyAvailableValidation((RequestBean) target, errors);
    this.addNumericValidation((RequestBean) target, errors);
  }

  private void sanitise(RequestBean request, Errors errors) {

    if (errors.getFieldErrorCount("from") == 0) {
      request.setFrom(Jsoup.clean(request.getFrom(), Whitelist.basic()));
    }

    if (errors.getFieldErrorCount("to") == 0) {
      request.setTo(Jsoup.clean(request.getTo(), Whitelist.basic()));
    }
    if (errors.getFieldErrorCount("amount") == 0) {
      request.setAmount(Jsoup.clean(request.getAmount(), Whitelist.basic()));
    }
  }

  private void addEmptyFieldValidation(Errors errors) {
    // Check if required fields are available
    ValidationUtils.rejectIfEmptyOrWhitespace(
        errors, "from", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED);
    ValidationUtils.rejectIfEmptyOrWhitespace(
        errors, "to", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED);
    ValidationUtils.rejectIfEmptyOrWhitespace(
        errors, "amount", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED);
  }

  private void addCurrencyAvailableValidation(RequestBean request, Errors errors) {
    // Check if currencies are available from the provider, prioritise "Not exists" errors above
    if (errors.getFieldErrorCount("from") == 0) {
      addIfNotInCurrencies(errors, "from", request.getFrom());
    }

    if (errors.getFieldErrorCount("to") == 0) {
      addIfNotInCurrencies(errors, "to", request.getTo());
    }
  }

  private void addIfNotInCurrencies(Errors errors, String currencyCodeField, String currencyCode) {
    for (CurrencySymbol symbol : this.availableCurrencies) {
      if (symbol.getCode().equals(currencyCode)) {
        return;
      }
    }

    errors.rejectValue(
        currencyCodeField, ERROR_CODE_FIELD_NOT_AVAILABLE, MSG_THIS_CURRENCY_IS_NOT_AVAILABLE);
  }

  private void addNumericValidation(RequestBean request, Errors errors) {
    if (errors.getFieldErrorCount("amount") == 0) {
      if (request.getAmountNumeric().equals(BigDecimal.ZERO)) {
        errors.rejectValue("amount", FIELD_NOT_NUMERIC, AMOUNT_FIELD_SHOULD_BE_NUMERIC);
      }
    }
  }
}
