package se.kurt.rest.converter.providers;

import java.util.Optional;
import java.util.Set;
import se.kurt.rest.converter.model.CurrencySymbol;
import se.kurt.rest.converter.model.RequestBean;

/**
 * Interface defining the backend provider services
 */
public interface ExchangeRateProvider {

  public Optional<? extends ProviderResponseBean> getLatestRate(RequestBean bean);

  public Optional<? extends ProviderResponseBean> convertAmount(RequestBean bean);

  public Set<CurrencySymbol> fetchAvailableCurrencies();
}
