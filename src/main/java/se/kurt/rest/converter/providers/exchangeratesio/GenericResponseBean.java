package se.kurt.rest.converter.providers.exchangeratesio;

import se.kurt.rest.converter.providers.ProviderResponseBean;

public class GenericResponseBean implements ProviderResponseBean {

  private ErrorResponseBean error;

  private boolean success;

  public GenericResponseBean(ErrorResponseBean error) {
    this.error = error;
  }

  public ErrorResponseBean getError() {
    return error;
  }

  public void setError(ErrorResponseBean error) {
    this.error = error;
  }

  @Override
  public boolean isSuccess() {
    return this.success;
  }
}
