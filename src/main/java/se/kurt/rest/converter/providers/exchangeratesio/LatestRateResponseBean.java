package se.kurt.rest.converter.providers.exchangeratesio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import se.kurt.rest.converter.providers.ProviderResponseBean;

/**
 * Mapper object for the latest rate response from the provider exchangeratesapi.io
 *
 * <p>Example response;
 * {"success":true,"timestamp":1626384423,"base":"EUR","date":"2021-07-15","rates":{"GBP":0.854162,"JPY":129.736675,"EUR":1}}
 */
public class LatestRateResponseBean implements ProviderResponseBean {

  private ErrorResponseBean error;

  private Map<String, BigDecimal> rates;
  private Date date;

  private String base;

  private long timestamp;
  private boolean success;

  public BigDecimal getLatestRateFor(String targetCurrency) {
    return this.getRates().get(targetCurrency);
  }

  @Override
  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public ErrorResponseBean getError() {
    return error;
  }

  public void setError(ErrorResponseBean error) {
    this.error = error;
  }

  public String getBase() {
    return base;
  }

  public void setBase(String base) {
    this.base = base;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Map<String, BigDecimal> getRates() {
    return rates;
  }

  public void setRates(Map<String, BigDecimal> rates) {
    this.rates = rates;
  }

  @Override
  public String toString() {
    return "LatestRateResponseBean{"
        + "error="
        + error
        + ", rates="
        + rates
        + ", date="
        + date
        + ", base='"
        + base
        + '\''
        + ", timestamp="
        + timestamp
        + ", success="
        + success
        + '}';
  }
}
