package se.kurt.rest.converter.providers.exchangeratesio;

import java.math.BigDecimal;
import java.util.Date;
import se.kurt.rest.converter.providers.ProviderResponseBean;

/**
 * Mapper object for the convert amount response from the provider exchangeratesapi.io
 *
 * <p>Example response;
 *
 * <p>{"success": true, "query": { "from": "GBP", "to": "JPY", "amount": 25 }, "info": {
 * "timestamp": 1519328414, "rate": 148.972231 }, "historical": "" "date": "2018-02-22" "result":
 * 3724.305775 }
 */
public class ConverterResponseBean implements ProviderResponseBean {

  private boolean success;

  private Date date;
  private Query query;
  private Info info;

  private BigDecimal result;

  private boolean historical;

  public BigDecimal getLatestRateFor(String targetCurrency) {
    return this.getInfo().getRate();
  }

  @Override
  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public void setError(ErrorResponseBean errorResponse) {
    this.setError(errorResponse);
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Query getQuery() {
    return query;
  }

  public void setQuery(Query query) {
    this.query = query;
  }

  public Info getInfo() {
    return info;
  }

  public void setInfo(Info info) {
    this.info = info;
  }

  public BigDecimal getResult() {
    return result;
  }

  public void setResult(BigDecimal result) {
    this.result = result;
  }

  public boolean isHistorical() {
    return historical;
  }

  public void setHistorical(boolean historical) {
    this.historical = historical;
  }

  @Override
  public String toString() {
    return "ConverterResponseBean{"
        + "success="
        + success
        + ", date="
        + date
        + ", query="
        + query
        + ", info="
        + info
        + ", result="
        + result
        + ", historical="
        + historical
        + '}';
  }
}

class Query {
  private String from;
  private String to;
  private BigDecimal amount;

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public String toString() {
    return "Query{" + "from='" + from + '\'' + ", to='" + to + '\'' + ", amount=" + amount + '}';
  }
}

class Info {
  private BigDecimal rate;
  private long timestamp;

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  @Override
  public String toString() {
    return "Info{" + "rate=" + rate + ", timestamp=" + timestamp + '}';
  }
}
