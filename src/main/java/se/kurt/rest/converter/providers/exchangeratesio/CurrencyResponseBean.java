package se.kurt.rest.converter.providers.exchangeratesio;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import se.kurt.rest.converter.model.CurrencySymbol;
import se.kurt.rest.converter.providers.ProviderResponseBean;

/**
 * Mapper object for the available currencies response from the provider exchangeratesapi.io
 *
 * <p>Example response; { "success": true, "symbols": { "AED": "United Arab Emirates Dirham", "AFN":
 * "Afghan Afghani", "ALL": "Albanian Lek", "AMD": "Armenian Dram", [...] } }
 */
public class CurrencyResponseBean implements ProviderResponseBean {

  private ErrorResponseBean error;

  private Map<String, String> symbols;
  private boolean success;

  public Set<CurrencySymbol> populateCurrencyCodeSet() {
    final Set<CurrencySymbol> currencies = new HashSet<>();

    this.getSymbols().forEach((k, v) -> currencies.add(new CurrencySymbol(k, v)));
    return currencies;
  }

  public Map<String, String> getSymbols() {
    return symbols;
  }

  public void setSymbols(Map<String, String> symbols) {
    this.symbols = symbols;
  }

  @Override
  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public ErrorResponseBean getError() {
    return error;
  }

  public void setError(ErrorResponseBean error) {
    this.error = error;
  }

  @Override
  public String toString() {
    return "CurrencyResponseBean{" + "symbols=" + symbols + ", success=" + success + '}';
  }
}
