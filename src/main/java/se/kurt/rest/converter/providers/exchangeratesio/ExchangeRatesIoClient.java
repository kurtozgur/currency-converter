package se.kurt.rest.converter.providers.exchangeratesio;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import se.kurt.rest.converter.currencyconverter.ConverterRestController;
import se.kurt.rest.converter.model.CurrencySymbol;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.model.properties.ExchangeRateIoServerProperties;
import se.kurt.rest.converter.providers.ExchangeRateProvider;
import se.kurt.rest.converter.providers.ProviderResponseBean;

/**
 * Singleton client instance to make requests against the exchangeratesapi.io service.
 *
 * <p>OkHttp documentation suggests that it performs best when you create a single OkHttpClient
 * instance and reuse it for all HTTP calls
 */
@Component
public class ExchangeRatesIoClient implements ExchangeRateProvider {

  private static final Logger logger = LoggerFactory.getLogger(ConverterRestController.class);

  private final ExchangeRateIoServerProperties properties;

  private static OkHttpClient restClient;

  private String converterUrl;
  private String latestRateUrl;
  private String availableCurrenciesUrl;

  @Autowired
  public ExchangeRatesIoClient(ExchangeRateIoServerProperties properties) {
    this.properties = properties;
    this.makeUrls();
    this.restClient = setOkHttpClient();
  }

  private void makeUrls() {
    this.makeConverterUrl();
    this.makeLatestRateUrl();
    this.makeAvailableCurrenciesUrl();
  }

  private OkHttpClient setOkHttpClient() {
    File cacheDir = new File("/cache", "http_cache");
    long maxCacheSize = 1L * 1024L * 1024L; // 1 MiB

    OkHttpClient restClient =
        new OkHttpClient.Builder().cache(new Cache(cacheDir, maxCacheSize)).build();

    return restClient;
  }

  // e.g.: http://api.exchangeratesapi.io/v1/convert?access_key=%s&from=%s&to=%s&amount=%s
  private void makeConverterUrl() {
    this.converterUrl =
        this.properties.getUrl()
            + this.properties.getConvertEndpoint()
            + "?"
            + (this.properties.getAccessKey() + "=" + this.properties.getAccessKeyValue())
            + "&"
            + (this.properties.getSourceCurrency() + "=%s")
            + "&"
            + (this.properties.getTargetCurrency() + "=%s")
            + "&"
            + (this.properties.getAmount() + "=%s");
  }

  // e.g.: http://api.exchangeratesapi.io/v1/latest?access_key=121213base=USD&symbols=GBP
  private void makeLatestRateUrl() {
    this.latestRateUrl =
        this.properties.getUrl()
            + this.properties.getLatestRateEndpoint()
            + "?"
            + (this.properties.getAccessKey() + "=" + this.properties.getAccessKeyValue())
            + "&"
            + (this.properties.getRateFrom() + "=%s")
            + "&"
            + (this.properties.getRateTo() + "=%s");
  }

  // e.g.: https://api.exchangeratesapi.io/v1/symbols?access_key=API_KEY
  private void makeAvailableCurrenciesUrl() {
    this.availableCurrenciesUrl =
        this.properties.getUrl()
            + this.properties.getAvailableCurrenciesEndpoint()
            + "?"
            + (this.properties.getAccessKey() + "=" + this.properties.getAccessKeyValue());
  }

  @Override
  @Cacheable("rates")
  public Optional<LatestRateResponseBean> getLatestRate(RequestBean bean) {
    String queryUrl = String.format(this.latestRateUrl, bean.getFrom(), bean.getTo());

    Optional<LatestRateResponseBean> responseBean =
        (Optional<LatestRateResponseBean>)
            this.getResponseBeanFromUrl(LatestRateResponseBean.class, queryUrl);

    return responseBean.isPresent() ? responseBean : Optional.empty();
  }

  @Override
  @Cacheable("currencies")
  public Set<CurrencySymbol> fetchAvailableCurrencies() {
    Optional<CurrencyResponseBean> responseBean =
        (Optional<CurrencyResponseBean>)
            this.getResponseBeanFromUrl(CurrencyResponseBean.class, this.availableCurrenciesUrl);

    Set<CurrencySymbol> currencies = new HashSet<>();
    if (responseBean.isPresent()) {
      currencies = responseBean.get().populateCurrencyCodeSet();
    }
    return currencies;
  }

  /**
   * ! Not implemented fully for this service
   *
   * @param bean
   * @return
   */
  @Override
  @Cacheable("conversions")
  public Optional<ConverterResponseBean> convertAmount(RequestBean bean) {
    String queryUrl =
        String.format(this.converterUrl, bean.getFrom(), bean.getTo(), bean.getAmountNumeric());
    //
    // TO DO
    //
    this.getResponseBeanFromUrl(ConverterResponseBean.class, queryUrl).get();

    return Optional.empty();
  }

  private Optional<? extends ProviderResponseBean> getResponseBeanFromUrl(
      Class<? extends ProviderResponseBean> responseClass, String queryUrl) {

    String response = null;

    try {
      response = getUrl(queryUrl);
      ProviderResponseBean responseBean = new ObjectMapper().readValue(response, responseClass);

      return Optional.of(responseBean);

    } catch (JsonProcessingException e) {
      logger.error(
          "The response from exchangerates.io server is in an unexpected format. [Response: {}]",
          response,
          e);
    } catch (Exception ge) {
      logger.error(
          "An error occurred while processing the response from exchangerates.io server. [Response: {}]",
          response,
          ge);
    }

    return Optional.empty();
  }

  private String getUrl(String url) throws IOException {

    // Exchange rates change rapidly, should not have a long cache time
    Request request =
        new Request.Builder()
            .url(url)
            .cacheControl(new CacheControl.Builder().maxStale(5, TimeUnit.SECONDS).build())
            .build();

    try (Response response = this.restClient.newCall(request).execute()) {
      return response.body().string();

    } catch (IOException e) {
      logger.error(
          "An error occurred while sending the request to exchangerates.io server. [Request: {}]",
          request,
          e);
      throw new IOException();
    }
  }

  // For testing purposes
  protected String getLatestRateUrl() {
    return latestRateUrl;
  }

  protected String getAvailableCurrenciesUrl() {
    return availableCurrenciesUrl;
  }
}
