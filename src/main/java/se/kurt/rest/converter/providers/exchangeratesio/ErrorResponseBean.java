package se.kurt.rest.converter.providers.exchangeratesio;

/**
 * Holds the error response from exchangereates.io
 *
 * <p>e.g.: {"error":{"code":"function_access_restricted","message":"Access Restricted - Your //
 * current Subscription Plan does not support this API Function."}}
 */
public class ErrorResponseBean {

  private String code;
  private String message;

  public ErrorResponseBean() {}

  public ErrorResponseBean(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "ErrorResponseBean{" + "code='" + code + '\'' + ", message='" + message + '\'' + '}';
  }
}
