package se.kurt.rest.converter.providers;

/** Mostly a marker interface */
public interface ProviderResponseBean {

  public boolean isSuccess();
}
