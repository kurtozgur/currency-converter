package se.kurt.rest.converter.model;

import java.math.BigDecimal;

/** Bean that holds the query params that will be sent to backend REST service */
public class RequestBean {

  private String from;
  private String to;
  private String amount;

  public RequestBean() {}

  public RequestBean(String from, String to, String amount) {
    this.from = from;
    this.to = to;
    this.setAmount(amount);
  }

  private BigDecimal amountNumeric;

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getAmount() {
    return amount;
  }

  public BigDecimal getAmountNumeric() {
    return amountNumeric;
  }

  public void setAmount(String amount) {
    this.amount = amount;

    // Convert to BigDecimal
    try {
      this.amountNumeric = new BigDecimal(amount);
    } catch (Exception e) {
      this.amountNumeric = BigDecimal.ZERO;
    }
  }

  @Override
  public String toString() {
    return "RequestBean{"
        + "from='"
        + from
        + '\''
        + ", to='"
        + to
        + '\''
        + ", amount="
        + amount
        + '}';
  }
}
