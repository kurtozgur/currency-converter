package se.kurt.rest.converter.model;

import java.math.BigDecimal;
import se.kurt.rest.converter.Util;

public class SuccessResponse {

  private final boolean success = true;

  private String sourceCurrency;
  private String targetCurrency;
  private String amount;
  private String rate;
  private String convertedAmount;

  public SuccessResponse(RequestBean request, BigDecimal convertedAmount, BigDecimal rate) {

    this.convertedAmount = Util.bigDecimalToString(convertedAmount);
    this.rate = Util.bigDecimalToString(rate);

    // Request (input) query params
    this.sourceCurrency = request.getFrom();
    this.targetCurrency = request.getTo();
    this.amount = request.getAmount();
  }

  public String getSourceCurrency() {
    return sourceCurrency;
  }

  public String getTargetCurrency() {
    return targetCurrency;
  }

  public String getAmount() {
    return amount;
  }

  public String getConvertedAmount() {
    return convertedAmount;
  }

  public String getRate() {
    return rate;
  }

  public boolean isSuccess() {
    return success;
  }
}
