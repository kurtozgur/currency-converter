package se.kurt.rest.converter.model.properties;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix = "exchangerates-io")
@ConstructorBinding
@Validated
public class ExchangeRateIoServerProperties {

  private String url;
  @NotNull private String convertEndpoint;
  @NotNull private String latestRateEndpoint;
  @NotNull private String availableCurrenciesEndpoint;

  private String accessKey;
  private String accessKeyValue;
  private String sourceCurrency;
  private String targetCurrency;
  private String amount;
  private String rateFrom;
  private String rateTo;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @NotNull
  public String getConvertEndpoint() {
    return convertEndpoint;
  }

  public void setConvertEndpoint(@NotNull String convertEndpoint) {
    this.convertEndpoint = convertEndpoint;
  }

  @NotNull
  public String getLatestRateEndpoint() {
    return latestRateEndpoint;
  }

  public void setLatestRateEndpoint(@NotNull String latestRateEndpoint) {
    this.latestRateEndpoint = latestRateEndpoint;
  }

  @NotNull
  public String getAvailableCurrenciesEndpoint() {
    return availableCurrenciesEndpoint;
  }

  public void setAvailableCurrenciesEndpoint(@NotNull String availableCurrenciesEndpoint) {
    this.availableCurrenciesEndpoint = availableCurrenciesEndpoint;
  }

  public String getAccessKey() {
    return accessKey;
  }

  public void setAccessKey(String accessKey) {
    this.accessKey = accessKey;
  }

  public String getAccessKeyValue() {
    return accessKeyValue;
  }

  public void setAccessKeyValue(String accessKeyValue) {
    this.accessKeyValue = accessKeyValue;
  }

  public String getSourceCurrency() {
    return sourceCurrency;
  }

  public void setSourceCurrency(String sourceCurrency) {
    this.sourceCurrency = sourceCurrency;
  }

  public String getTargetCurrency() {
    return targetCurrency;
  }

  public void setTargetCurrency(String targetCurrency) {
    this.targetCurrency = targetCurrency;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getRateFrom() {
    return rateFrom;
  }

  public void setRateFrom(String rateFrom) {
    this.rateFrom = rateFrom;
  }

  public String getRateTo() {
    return rateTo;
  }

  public void setRateTo(String rateTo) {
    this.rateTo = rateTo;
  }
}
