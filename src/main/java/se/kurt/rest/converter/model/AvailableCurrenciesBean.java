package se.kurt.rest.converter.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.kurt.rest.converter.providers.ExchangeRateProvider;

/**
 * Retrieves the available currencies on the backend provider with the help of the injected client
 */
@Component
public class AvailableCurrenciesBean {

  private final Set<CurrencySymbol> availableCurrencies;
  private final List<CurrencySymbol> availableCurrenciesList;

  @Autowired
  public AvailableCurrenciesBean(ExchangeRateProvider client) {
    this.availableCurrencies = client.fetchAvailableCurrencies();
    this.availableCurrenciesList = new ArrayList<>(this.availableCurrencies);
    // sort alphabetically by name
    Collections.sort(this.availableCurrenciesList);
  }

  public Set<CurrencySymbol> getSet() {
    return this.availableCurrencies;
  }

  public List<CurrencySymbol> getList() {
    return this.availableCurrenciesList;
  }
}
