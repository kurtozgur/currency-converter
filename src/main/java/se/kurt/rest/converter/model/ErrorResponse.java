package se.kurt.rest.converter.model;

public class ErrorResponse {

  private final boolean success = false;

  private String message;

  public ErrorResponse(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public boolean isSuccess() {
    return success;
  }
}
