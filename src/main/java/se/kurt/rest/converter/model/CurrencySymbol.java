package se.kurt.rest.converter.model;

import java.util.Objects;
import org.jetbrains.annotations.NotNull;

public class CurrencySymbol implements Comparable {

  private String code;
  private String name;

  public CurrencySymbol(String code, String name) {
    this.code = code;
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CurrencySymbol that = (CurrencySymbol) o;
    return Objects.equals(code, that.code) && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, name);
  }

  @Override
  public String toString() {
    return "CurrencySymbol{" + "code='" + code + '\'' + ", name='" + name + '\'' + '}';
  }

  @Override
  public int compareTo(@NotNull Object o) {
    if (this.name == null) {
      return -1;
    }

    return this.getName().compareTo(((CurrencySymbol) o).getName());
  }
}
