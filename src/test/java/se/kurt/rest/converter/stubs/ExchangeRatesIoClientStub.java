package se.kurt.rest.converter.stubs;

import se.kurt.rest.converter.model.properties.ExchangeRateIoServerProperties;
import se.kurt.rest.converter.providers.exchangeratesio.ExchangeRatesIoClient;

/**
 * A stub implementation for exchange rate io client that sets up the injected mockWebServer object
 * from stubbed backend server properties
 */
public class ExchangeRatesIoClientStub {
  public ExchangeRatesIoClient getStub(ExchangeRateIoServerProperties stubProperties) {
    return new ExchangeRatesIoClient(stubProperties);
  }
}
