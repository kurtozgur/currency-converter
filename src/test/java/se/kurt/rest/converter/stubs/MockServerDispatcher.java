package se.kurt.rest.converter.stubs;

import static se.kurt.rest.converter.MessageConstants.CODE_101;
import static se.kurt.rest.converter.MessageConstants.CODE_103;
import static se.kurt.rest.converter.MessageConstants.CODE_106;
import static se.kurt.rest.converter.MessageConstants.NO_API_KEY_WAS_SPECIFIED_OR_AN_INVALID_API_KEY_WAS_SPECIFIED;
import static se.kurt.rest.converter.MessageConstants.THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS;
import static se.kurt.rest.converter.MessageConstants.THE_REQUESTED_API_ENDPOINT_DOES_NOT_EXIST;
import static se.kurt.rest.converter.VerificationUtils.EUR;
import static se.kurt.rest.converter.VerificationUtils.EUR_GBP_RATE;
import static se.kurt.rest.converter.VerificationUtils.EUR_SEK_RATE;
import static se.kurt.rest.converter.VerificationUtils.GBP;
import static se.kurt.rest.converter.VerificationUtils.SEK;
import static se.kurt.rest.converter.VerificationUtils.getValidCurrencySetSample;
import static se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub.ACCESS_KEY;
import static se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub.ACCESS_KEY_VALUE;
import static se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub.AVAILABLE_CURRENCIES_ENDPOINT;
import static se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub.LATEST_RATE_ENDPOINT;
import static se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub.RATE_FROM;
import static se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub.RATE_TO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;
import org.jetbrains.annotations.NotNull;
import se.kurt.rest.converter.providers.ProviderResponseBean;
import se.kurt.rest.converter.providers.exchangeratesio.CurrencyResponseBean;
import se.kurt.rest.converter.providers.exchangeratesio.ErrorResponseBean;
import se.kurt.rest.converter.providers.exchangeratesio.GenericResponseBean;
import se.kurt.rest.converter.providers.exchangeratesio.LatestRateResponseBean;

/** Mock response producer for exchangeratesapi.io */
public class MockServerDispatcher extends Dispatcher {

  /** Dispatch pre-defined responses on given url and query parameter conditions */
  @NotNull
  @Override
  public MockResponse dispatch(@NotNull RecordedRequest request) throws InterruptedException {

    try {
      if (request.getPath().startsWith(LATEST_RATE_ENDPOINT + "?")) {
        return this.produceLatestRateResponse(request);

      } else if (request.getPath().startsWith(AVAILABLE_CURRENCIES_ENDPOINT + "?")) {
        return this.produceAvailableCurrenciesResponse(request);

      } else {
        // error response

        return new MockResponse()
            .setBody(
                new ObjectMapper()
                    .writeValueAsString(
                        new GenericResponseBean(
                            new ErrorResponseBean(
                                CODE_103, THE_REQUESTED_API_ENDPOINT_DOES_NOT_EXIST))))
            .addHeader("Content-Type", "application/json");
      }

    } catch (JsonProcessingException e) {
      throw new InterruptedException(THE_REQUESTED_API_ENDPOINT_DOES_NOT_EXIST);
    }
  }

  private MockResponse produceAvailableCurrenciesResponse(RecordedRequest request)
      throws JsonProcessingException {

    CurrencyResponseBean bean = new CurrencyResponseBean();
    String[] paramsNeeded = new String[] {ACCESS_KEY};

    if (this.necessaryParamsExist(request.getRequestUrl().queryParameterNames(), paramsNeeded)) {

      String accessKey = request.getRequestUrl().queryParameter(ACCESS_KEY);
      if (!accessKey.equals(ACCESS_KEY_VALUE)) {
        // access key error
        bean.setError(
            new ErrorResponseBean(
                CODE_101, NO_API_KEY_WAS_SPECIFIED_OR_AN_INVALID_API_KEY_WAS_SPECIFIED));
      } else {
        bean.setSuccess(true);
        bean.setSymbols(
            getValidCurrencySetSample().stream()
                .collect(Collectors.toMap(s -> s.getCode(), s -> s.getName())));
      }

      return new MockResponse()
          .setBody(new ObjectMapper().writeValueAsString(bean))
          .addHeader("Content-Type", "application/json");
    } else {
      // missing params
      bean.setError(
          new ErrorResponseBean(CODE_106, THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS));
      return new MockResponse()
          .setBody(new ObjectMapper().writeValueAsString(bean))
          .addHeader("Content-Type", "application/json");
    }
  }

  // Mock server will only provide rates for EUR -> GBP and EUR -> SEK
  private MockResponse produceLatestRateResponse(@NotNull RecordedRequest request)
      throws JsonProcessingException {

    LatestRateResponseBean responseBean = new LatestRateResponseBean();
    String[] paramsNeeded = new String[] {ACCESS_KEY, RATE_FROM, RATE_TO};

    if (this.necessaryParamsExist(request.getRequestUrl().queryParameterNames(), paramsNeeded)) {

      String accessKey = request.getRequestUrl().queryParameter(ACCESS_KEY);
      String from = request.getRequestUrl().queryParameter(RATE_FROM);
      String to = request.getRequestUrl().queryParameter(RATE_TO);

      if (!accessKey.equals(ACCESS_KEY_VALUE)) {
        // access key error
        responseBean.setError(
            new ErrorResponseBean(
                CODE_101, NO_API_KEY_WAS_SPECIFIED_OR_AN_INVALID_API_KEY_WAS_SPECIFIED));

        return mockResponseFromBean(responseBean);
      } else {

        if (!from.equals("EUR")) {
          // parameter error
          responseBean.setError(
              new ErrorResponseBean(CODE_106, THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS));
        } else {

          if (!(to.equals("GBP") || to.equals("SEK"))) {
            // parameter error
            responseBean.setError(
                new ErrorResponseBean(CODE_106, THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS));

            return mockResponseFromBean(responseBean);
          }

          BigDecimal toRate = BigDecimal.ONE;
          if (to.equals(GBP)) {
            toRate = new BigDecimal(EUR_GBP_RATE);

          } else if (to.equals(SEK)) {
            toRate = new BigDecimal(EUR_SEK_RATE);
          }

          Map rateMap = new HashMap<String, BigDecimal>();
          rateMap.put(to, toRate);

          responseBean.setSuccess(true);
          responseBean.setTimestamp(System.currentTimeMillis());
          responseBean.setBase(EUR);
          responseBean.setDate(new Date());
          responseBean.setRates(rateMap);
        }
        return mockResponseFromBean(responseBean);
      }
    } else {
      // missing params
      responseBean.setError(
          new ErrorResponseBean(CODE_106, THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS));
      return mockResponseFromBean(responseBean);
    }
  }

  // Check if necessary query parameters exist, excess parameters do not matter
  private boolean necessaryParamsExist(Set<String> params, String[] needed) {
    for (String p : needed) {
      if (!params.contains(p)) {
        return false;
      }
    }

    return true;
  }

  private MockResponse mockResponseFromBean(ProviderResponseBean responseBean)
      throws JsonProcessingException {
    return new MockResponse()
        .setBody(new ObjectMapper().writeValueAsString(responseBean))
        .addHeader("Content-Type", "application/json");
  }
}
