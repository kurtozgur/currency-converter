package se.kurt.rest.converter.stubs;

import java.io.IOException;
import okhttp3.mockwebserver.MockWebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.kurt.rest.converter.model.properties.ExchangeRateIoServerProperties;
import se.kurt.rest.converter.providers.exchangeratesio.ExchangeRatesIoClient;

@Configuration
public class BeanConfig {

  @Bean
  public ExchangeRatesIoClient exchangeRatesIoClient() throws IOException {
    // Get injected mock server's url for properties
    MockWebServer mockWebServer = mockWebServer();

    String serverUrl = mockWebServer.url("").toString();
    serverUrl = serverUrl.substring(0, serverUrl.length() - 1);

    ExchangeRateIoServerProperties stubProperties =
        new ExchangeRateIoServerPropertiesStub().getStub(serverUrl);

    return new ExchangeRatesIoClientStub().getStub(stubProperties);
  }

  /**
   * Set up a mock web server that prepares the response for a call to get all currencies This is
   * needed to make the validator work since it gets the currencies on startup and runs before
   * anything else (i.e. the client should know about the backend provider)
   */
  @Bean
  public MockWebServer mockWebServer() throws IOException {
    MockWebServer mockWebServer = new MockWebServer();
    mockWebServer.setDispatcher(new MockServerDispatcher());

    return mockWebServer;
  }
}
