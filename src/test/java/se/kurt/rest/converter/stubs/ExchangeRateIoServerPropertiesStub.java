package se.kurt.rest.converter.stubs;

import se.kurt.rest.converter.model.properties.ExchangeRateIoServerProperties;

/** A properties class that has dummy endpoint and mock server information */
public class ExchangeRateIoServerPropertiesStub {

  protected static final String ACCESS_KEY = "access_key";
  protected static final String ACCESS_KEY_VALUE = "A123456F";
  protected static final String CONVERT_ENDPOINT = "/converter";
  protected static final String LATEST_RATE_ENDPOINT = "/rater";
  protected static final String AVAILABLE_CURRENCIES_ENDPOINT = "/currencies";
  protected static final String SOURCE_CURRENCY = "from";
  protected static final String TARGET_CURRENCY = "to";
  protected static final String AMOUNT = "amount";
  protected static final String RATE_FROM = "base";
  protected static final String RATE_TO = "symbol";

  public ExchangeRateIoServerProperties getStub(String baseUrl) {
    ExchangeRateIoServerProperties properties = new ExchangeRateIoServerProperties();

    properties.setUrl(baseUrl);
    properties.setAccessKey(ACCESS_KEY);
    properties.setAccessKeyValue(ACCESS_KEY_VALUE);
    properties.setConvertEndpoint(CONVERT_ENDPOINT);
    properties.setLatestRateEndpoint(LATEST_RATE_ENDPOINT);
    properties.setAvailableCurrenciesEndpoint(AVAILABLE_CURRENCIES_ENDPOINT);
    properties.setSourceCurrency(SOURCE_CURRENCY);
    properties.setTargetCurrency(TARGET_CURRENCY);
    properties.setAmount(AMOUNT);
    properties.setRateFrom(RATE_FROM);
    properties.setRateTo(RATE_TO);

    return properties;
  }
}
