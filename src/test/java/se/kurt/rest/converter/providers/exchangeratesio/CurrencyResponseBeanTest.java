package se.kurt.rest.converter.providers.exchangeratesio;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static se.kurt.rest.converter.VerificationUtils.getValidCurrencySetSample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.kurt.rest.converter.model.CurrencySymbol;

class CurrencyResponseBeanTest {

  private CurrencyResponseBean bean;

  @BeforeEach
  void setUp() {
    this.bean = new CurrencyResponseBean();
  }

  @Test
  void testCurrencySymbolsPopulatedCorrectly() {

    Set<CurrencySymbol> expectedCurrencies = getValidCurrencySetSample();

    this.prepareBeanForAssertion(new ArrayList<>(expectedCurrencies));
    Set<CurrencySymbol> actual = this.bean.populateCurrencyCodeSet();

    assertEquals(expectedCurrencies.size(), actual.size());
    assertThat(actual).containsExactlyInAnyOrderElementsOf(expectedCurrencies);
  }

  @Test
  void testCurrencySymbolsPopulatedCorrectlyWhenServerReturnsDuplicate() {

    Set<CurrencySymbol> expectedCurrencies = getValidCurrencySetSample();

    List<CurrencySymbol> listFromProvider = new ArrayList<>(expectedCurrencies);
    listFromProvider.add(new CurrencySymbol("GBP", "British Money"));
    listFromProvider.add(new CurrencySymbol("EUR", "European Money"));

    this.prepareBeanForAssertion(listFromProvider);
    Set<CurrencySymbol> actual = this.bean.populateCurrencyCodeSet();

    assertEquals(expectedCurrencies.size(), actual.size());
    assertThat(actual).containsExactlyInAnyOrderElementsOf(expectedCurrencies);
  }

  @Test
  void testCurrencySymbolsAreEmpty() {

    Set<CurrencySymbol> expectedCurrencies = new HashSet<>();
    this.prepareBeanForAssertion(new ArrayList<>(expectedCurrencies));

    assertTrue(this.bean.populateCurrencyCodeSet().isEmpty());
  }

  // Set currencies and populate the set
  private void prepareBeanForAssertion(List<CurrencySymbol> currencies) {
    Map<String, String> map = new HashMap<>();
    currencies.forEach(symbol -> map.put(symbol.getCode(), symbol.getName()));

    this.bean.setSymbols(map);
  }
}
