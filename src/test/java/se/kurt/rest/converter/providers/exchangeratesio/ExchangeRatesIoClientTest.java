package se.kurt.rest.converter.providers.exchangeratesio;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static se.kurt.rest.converter.MessageConstants.CODE_101;
import static se.kurt.rest.converter.MessageConstants.CODE_103;
import static se.kurt.rest.converter.MessageConstants.CODE_106;
import static se.kurt.rest.converter.MessageConstants.NO_API_KEY_WAS_SPECIFIED_OR_AN_INVALID_API_KEY_WAS_SPECIFIED;
import static se.kurt.rest.converter.MessageConstants.THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS;
import static se.kurt.rest.converter.MessageConstants.THE_REQUESTED_API_ENDPOINT_DOES_NOT_EXIST;
import static se.kurt.rest.converter.VerificationUtils.getValidCurrencySetSample;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import se.kurt.rest.converter.model.CurrencySymbol;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.model.properties.ExchangeRateIoServerProperties;
import se.kurt.rest.converter.stubs.ExchangeRateIoServerPropertiesStub;
import se.kurt.rest.converter.stubs.MockServerDispatcher;

class ExchangeRatesIoClientTest {

  private MockWebServer mockRestServer;

  private ExchangeRateIoServerProperties properties;
  private ExchangeRatesIoClient provider;

  @BeforeEach
  void setUp() throws IOException {
    this.mockRestServer = new MockWebServer();
    this.mockRestServer.setDispatcher(new MockServerDispatcher());
    this.mockRestServer.start();

    String serverUrl = this.mockRestServer.url("").toString();
    // Strip last char "/"
    serverUrl = serverUrl.substring(0, serverUrl.length() - 1);

    this.properties = new ExchangeRateIoServerPropertiesStub().getStub(serverUrl);
    this.provider = new ExchangeRatesIoClient(this.properties);
  }

  @Test
  @Order(1)
  void testUrlsAreCorrect() {
    // Expected latest rate URL
    String expectedLatestRateUrl =
        this.properties.getUrl()
            + this.properties.getLatestRateEndpoint()
            + "?"
            + (this.properties.getAccessKey() + "=" + this.properties.getAccessKeyValue())
            + "&"
            + (this.properties.getRateFrom() + "=%s")
            + "&"
            + (this.properties.getRateTo() + "=%s");
    assertEquals(expectedLatestRateUrl, this.provider.getLatestRateUrl());

    // Expected available currencies URL
    String expectedCurrenciesUrl =
        this.properties.getUrl()
            + this.properties.getAvailableCurrenciesEndpoint()
            + "?"
            + (this.properties.getAccessKey() + "=" + this.properties.getAccessKeyValue());
    assertEquals(expectedCurrenciesUrl, this.provider.getAvailableCurrenciesUrl());
  }

  @Test
  void testFetchAvailableCurrenciesWithSuccess() throws InterruptedException {

    // Make the call via provider
    Set<CurrencySymbol> actualCurrencySet = this.provider.fetchAvailableCurrencies();
    Set<CurrencySymbol> expectedCurrencySet = getValidCurrencySetSample();

    // Verify return currencies
    assertEquals(expectedCurrencySet.size(), actualCurrencySet.size());
    assertThat(actualCurrencySet).containsExactlyInAnyOrderElementsOf(expectedCurrencySet);

    // Verify request was sent
    RecordedRequest recordedRequest = mockRestServer.takeRequest();

    assertEquals("GET", recordedRequest.getMethod());
    assertEquals(
        this.provider.getAvailableCurrenciesUrl(), recordedRequest.getRequestUrl().toString());
  }

  @Test
  void testGetLatestRateWithSuccess() throws InterruptedException {

    final BigDecimal gbpRate = new BigDecimal("0.854162");

    // Make the call via provider
    Optional<LatestRateResponseBean> serverResponse =
        this.provider.getLatestRate(new RequestBean("EUR", "GBP", null));
    assertTrue(serverResponse.isPresent());

    LatestRateResponseBean bean = serverResponse.get();
    assertTrue(bean.isSuccess());
    assertEquals(1, bean.getRates().size());
    assertTrue(bean.getLatestRateFor("GBP").equals(gbpRate));

    // Verify responseBean was sent
    RecordedRequest recordedRequest = mockRestServer.takeRequest();

    assertEquals("GET", recordedRequest.getMethod());
    assertEquals(
        String.format(this.provider.getLatestRateUrl(), "EUR", "GBP"),
        recordedRequest.getRequestUrl().toString());
  }

  @Test
  void testGetLatestRateWithUnavailableCurrencyShouldReturnError() throws InterruptedException {

    // Make the call via provider
    Optional<LatestRateResponseBean> serverResponse =
        this.provider.getLatestRate(new RequestBean("EUR", "ABC", null));
    assertTrue(serverResponse.isPresent());

    LatestRateResponseBean bean = serverResponse.get();
    assertFalse(bean.isSuccess());
    assertEquals(CODE_106, bean.getError().getCode());
    assertEquals(THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS, bean.getError().getMessage());

    // Verify responseBean was sent
    RecordedRequest recordedRequest = mockRestServer.takeRequest();

    assertEquals("GET", recordedRequest.getMethod());
    assertEquals(
        String.format(this.provider.getLatestRateUrl(), "EUR", "ABC"),
        recordedRequest.getRequestUrl().toString());
  }

  @Test
  void testGetLatestRateWithMissingParametersShouldReturnError() throws InterruptedException {

    // Make the call via provider
    Optional<LatestRateResponseBean> serverResponse =
        this.provider.getLatestRate(new RequestBean("EUR", "", null));
    assertTrue(serverResponse.isPresent());

    LatestRateResponseBean bean = serverResponse.get();
    assertFalse(bean.isSuccess());
    assertEquals(CODE_106, bean.getError().getCode());
    assertEquals(THE_CURRENT_REQUEST_DID_NOT_RETURN_ANY_RESULTS, bean.getError().getMessage());

    // Verify responseBean was sent
    RecordedRequest recordedRequest = mockRestServer.takeRequest();

    assertEquals("GET", recordedRequest.getMethod());
    assertEquals(
        String.format(this.provider.getLatestRateUrl(), "EUR", ""),
        recordedRequest.getRequestUrl().toString());
  }

  @Test
  void testUnavailableEndpointShouldReturnError() throws InterruptedException {

    // Get a new client with a dummy endpoint
    this.properties.setLatestRateEndpoint("/raterdummy");
    this.provider = new ExchangeRatesIoClient(this.properties);

    // Make the call via provider
    Optional<LatestRateResponseBean> serverResponse =
        this.provider.getLatestRate(new RequestBean("EUR", "GBP", "12"));
    assertTrue(serverResponse.isPresent());

    LatestRateResponseBean bean = serverResponse.get();
    assertFalse(bean.isSuccess());
    assertEquals(CODE_103, bean.getError().getCode());
    assertEquals(THE_REQUESTED_API_ENDPOINT_DOES_NOT_EXIST, bean.getError().getMessage());

    // Verify responseBean was sent
    RecordedRequest recordedRequest = mockRestServer.takeRequest();

    assertEquals("GET", recordedRequest.getMethod());
    assertEquals(
        String.format(this.provider.getLatestRateUrl(), "EUR", "GBP"),
        recordedRequest.getRequestUrl().toString());
  }

  @Test
  void testFaultyAccessKeyShouldReturnError() throws InterruptedException {

    // Get a new client with a faulty access key endpoint
    this.properties.setAccessKeyValue("12341");
    this.provider = new ExchangeRatesIoClient(this.properties);

    // Make the call via provider
    Optional<LatestRateResponseBean> serverResponse =
        this.provider.getLatestRate(new RequestBean("EUR", "GBP", "12"));
    assertTrue(serverResponse.isPresent());

    LatestRateResponseBean bean = serverResponse.get();
    assertFalse(bean.isSuccess());
    assertEquals(CODE_101, bean.getError().getCode());
    assertEquals(
        NO_API_KEY_WAS_SPECIFIED_OR_AN_INVALID_API_KEY_WAS_SPECIFIED, bean.getError().getMessage());

    // Verify responseBean was sent
    RecordedRequest recordedRequest = mockRestServer.takeRequest();

    assertEquals("GET", recordedRequest.getMethod());
    assertEquals(
        String.format(this.provider.getLatestRateUrl(), "EUR", "GBP"),
        recordedRequest.getRequestUrl().toString());
  }

  @Test
  @Ignore
  void testConvertAmount() {
    // Not implemented operation
  }
}
