package se.kurt.rest.converter.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class RequestBeanTest {

  @Test
  void testSetAmountWhenValidInput() {
    RequestBean bean = new RequestBean();

    bean.setAmount("1234.567");
    assertTrue(bean.getAmountNumeric().equals(new BigDecimal("1234.567")));

    bean.setAmount("0.0");
    assertTrue(bean.getAmountNumeric().equals(new BigDecimal("0.0")));
  }

  @Test
  void testSetAmountReturnsZero() {
    RequestBean bean = new RequestBean();

    bean.setAmount("123,4");
    assertTrue(bean.getAmountNumeric().equals(BigDecimal.ZERO));

    bean.setAmount("123.4.");
    assertTrue(bean.getAmountNumeric().equals(BigDecimal.ZERO));

    bean.setAmount("1a0.0");
    assertTrue(bean.getAmountNumeric().equals(BigDecimal.ZERO));

    bean.setAmount("");
    assertTrue(bean.getAmountNumeric().equals(BigDecimal.ZERO));
  }
}
