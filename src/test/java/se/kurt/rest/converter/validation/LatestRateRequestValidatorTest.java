package se.kurt.rest.converter.validation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static se.kurt.rest.converter.MessageConstants.AMOUNT_FIELD_SHOULD_BE_NUMERIC;
import static se.kurt.rest.converter.MessageConstants.ERROR_CODE_FIELD_NOT_AVAILABLE;
import static se.kurt.rest.converter.MessageConstants.ERROR_CODE_FIELD_REQUIRED;
import static se.kurt.rest.converter.MessageConstants.FIELD_NOT_NUMERIC;
import static se.kurt.rest.converter.MessageConstants.MSG_FIELD_SHOULD_BE_PROVIDED;
import static se.kurt.rest.converter.MessageConstants.MSG_THIS_CURRENCY_IS_NOT_AVAILABLE;

import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import se.kurt.rest.converter.VerificationUtils;
import se.kurt.rest.converter.model.AvailableCurrenciesBean;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.providers.ExchangeRateProvider;

class LatestRateRequestValidatorTest {

  private LatestRateRequestValidator validator;

  @BeforeEach
  void setUp() {
    ExchangeRateProvider provider = mock(ExchangeRateProvider.class);
    when(provider.fetchAvailableCurrencies())
        .thenReturn(VerificationUtils.getValidCurrencySetSample());

    AvailableCurrenciesBean currenciesBean = new AvailableCurrenciesBean(provider);
    this.validator = new LatestRateRequestValidator(currenciesBean);
  }

  @Test
  void testNoErrorsWhenAllFieldsValid() {
    RequestBean bean = new RequestBean("EUR", "GBP", "12.35");

    Errors errors = new BeanPropertyBindingResult(bean, "BeanWithValidFields");
    this.validator.validate(bean, errors);

    assertEquals(0, errors.getErrorCount());
  }

  @Test
  void testEmptyFieldValidationsWhenFieldsNotProvided() {
    RequestBean request = new RequestBean();

    // Expected fields with errors
    Tuple[] expectedErrors = {
      tuple("from", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED),
      tuple("to", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED),
      tuple("amount", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED),
    };

    verifyFieldErrors(request, expectedErrors);
  }

  @Test
  void testEmptyFieldValidationsWhenFieldsAreEmpty() {
    RequestBean request = new RequestBean("", "", "");

    // Expected fields with errors
    Tuple[] expectedErrors = {
      tuple("from", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED),
      tuple("to", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED),
      tuple("amount", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED),
    };

    verifyFieldErrors(request, expectedErrors);
  }

  @Test
  void testEmptyFieldValidationsWhenSomeFieldsNotProvided() {
    RequestBean request = new RequestBean("EUR", "GBP", null);

    // Expected fields with errors
    Tuple[] expectedErrors = {
      tuple("amount", ERROR_CODE_FIELD_REQUIRED, MSG_FIELD_SHOULD_BE_PROVIDED)
    };

    verifyFieldErrors(request, expectedErrors);
  }

  @Test
  void testCurrencyAvailableValidation() {
    RequestBean request = new RequestBean("TRY", "SEK", "12.11");

    // Expected fields with errors
    Tuple[] expectedErrors = {
      tuple("from", ERROR_CODE_FIELD_NOT_AVAILABLE, MSG_THIS_CURRENCY_IS_NOT_AVAILABLE),
      tuple("to", ERROR_CODE_FIELD_NOT_AVAILABLE, MSG_THIS_CURRENCY_IS_NOT_AVAILABLE)
    };

    verifyFieldErrors(request, expectedErrors);
  }

  @Test
  void testInvalidNumericAmount() {
    RequestBean request = new RequestBean("EUR", "GBP", "12F11");
    RequestBean requestComma = new RequestBean("EUR", "GBP", "12,11");

    // Expected fields with errors
    Tuple[] expectedErrors = {tuple("amount", FIELD_NOT_NUMERIC, AMOUNT_FIELD_SHOULD_BE_NUMERIC)};

    verifyFieldErrors(request, expectedErrors);
    verifyFieldErrors(requestComma, expectedErrors);
  }

  @Test
  void testValidNumericAmount() {
    // Integer amount
    RequestBean request = new RequestBean("EUR", "GBP", "1211");
    Errors errors = new BeanPropertyBindingResult(request, "BeanWithValidFields");
    this.validator.validate(request, errors);

    assertEquals(0, errors.getFieldErrorCount("amount"));

    // Amount with decimal part
    RequestBean requestDecimal = new RequestBean("EUR", "GBP", "12.11");
    Errors errorsDecimal = new BeanPropertyBindingResult(requestDecimal, "BeanWithValidFields");
    this.validator.validate(requestDecimal, errorsDecimal);

    assertEquals(0, errorsDecimal.getFieldErrorCount("amount"));

    // Zero
    RequestBean requestZero = new RequestBean("EUR", "GBP", "0.0");
    Errors errorsZero = new BeanPropertyBindingResult(requestZero, "BeanWithValidFields");
    this.validator.validate(requestZero, errorsZero);

    assertEquals(0, errorsZero.getFieldErrorCount("amount"));
  }

  private void verifyFieldErrors(RequestBean bean, Tuple[] expectedErrors) {

    Errors errors = new BeanPropertyBindingResult(bean, "BeanUnderTest");
    this.validator.validate(bean, errors);

    assertEquals(expectedErrors.length, errors.getErrorCount());

    assertThat(errors.getAllErrors().stream().map(this::makeErrorTuple).toArray())
        .containsExactlyInAnyOrder(expectedErrors);
  }

  private Tuple makeErrorTuple(ObjectError error) {
    return tuple(((FieldError) error).getField(), error.getCode(), error.getDefaultMessage());
  }
}
