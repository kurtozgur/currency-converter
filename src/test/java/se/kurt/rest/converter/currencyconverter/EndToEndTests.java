package se.kurt.rest.converter.currencyconverter;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static se.kurt.rest.converter.VerificationUtils.EUR;
import static se.kurt.rest.converter.VerificationUtils.EUR_GBP_RATE;
import static se.kurt.rest.converter.VerificationUtils.GBP;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.model.SuccessResponse;
import se.kurt.rest.converter.providers.ExchangeRateProvider;

/** Makes real calls to backend provider for a positive scenario */
@DirtiesContext
@WebMvcTest(ConverterRestController.class)
public class EndToEndTests {

  private static final BigDecimal EUR_GBP_RATE_NUMERIC = new BigDecimal(EUR_GBP_RATE);

  @Autowired private MockMvc mockMvc;
  @Autowired private ExchangeRateProvider rateProvider;

  @Test
  @Timeout(value = 60, unit = SECONDS) // Wait 1 min maximum
  void testSuccessfulConversion() throws Exception {
    String amount = "40";

    // Request object that will be created from the query params
    final RequestBean requestBean = new RequestBean();
    requestBean.setFrom(EUR);
    requestBean.setTo(GBP);
    requestBean.setAmount(amount);

    SuccessResponse expectedResponse =
        new SuccessResponse(
            requestBean,
            requestBean.getAmountNumeric().multiply(EUR_GBP_RATE_NUMERIC),
            EUR_GBP_RATE_NUMERIC);

    this.mockMvc
        .perform(get("/convert?from=" + EUR + "&to=" + GBP + "&amount=" + amount))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.sourceCurrency", is(EUR)))
        .andExpect(jsonPath("$.targetCurrency", is(GBP)))
        .andExpect(jsonPath("$.amount", is(amount)))
        .andExpect(jsonPath("$.rate", is(notNullValue())))
        .andExpect(jsonPath("$.convertedAmount", is(notNullValue())));
  }
}
