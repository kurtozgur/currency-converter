package se.kurt.rest.converter.currencyconverter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static se.kurt.rest.converter.MessageConstants.AMOUNT_FIELD_SHOULD_BE_NUMERIC;
import static se.kurt.rest.converter.MessageConstants.EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ;
import static se.kurt.rest.converter.MessageConstants.MSG_FIELD_SHOULD_BE_PROVIDED;
import static se.kurt.rest.converter.MessageConstants.MSG_THIS_CURRENCY_IS_NOT_AVAILABLE;
import static se.kurt.rest.converter.VerificationUtils.EUR;
import static se.kurt.rest.converter.VerificationUtils.EUR_GBP_RATE;
import static se.kurt.rest.converter.VerificationUtils.GBP;
import static se.kurt.rest.converter.VerificationUtils.TRY;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import se.kurt.rest.converter.model.ErrorResponse;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.model.SuccessResponse;
import se.kurt.rest.converter.providers.ExchangeRateProvider;

/** Integration tests for the REST service using a mock provider server */
@DirtiesContext
@WebMvcTest(ConverterRestController.class)
@ComponentScan(basePackages = {"se.kurt.rest.converter.stubs"})
class IntegrationTests {

  private static final BigDecimal EUR_GBP_RATE_NUMERIC = new BigDecimal(EUR_GBP_RATE);

  @Autowired private MockMvc mockMvc;
  @Autowired private MockWebServer mockRestServer;
  @Autowired private ExchangeRateProvider provider;

  @Test
  public void testUnderValidQueryParams() throws Exception {

    String amount = "40";

    // Request object that will be created from the query params
    final RequestBean requestBean = new RequestBean();
    requestBean.setFrom(EUR);
    requestBean.setTo(GBP);
    requestBean.setAmount(amount);

    SuccessResponse expectedResponse =
        new SuccessResponse(
            requestBean,
            requestBean.getAmountNumeric().multiply(EUR_GBP_RATE_NUMERIC),
            EUR_GBP_RATE_NUMERIC);

    this.mockMvc
        .perform(get("/convert?from=" + EUR + "&to=" + GBP + "&amount=" + amount))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(new ObjectMapper().writeValueAsString(expectedResponse)));
  }

  @Test
  public void testProviderServiceUnavailable() throws Exception {

    String amount = "40";

    // Shutdown backend provider service before call, wait for 6 seconds (caching is for 5 seconds)
    this.mockRestServer.shutdown();
    TimeUnit.SECONDS.sleep(6);

    this.mockMvc
        .perform(get("/convert?from=" + EUR + "&to=" + GBP + "&amount=" + amount))
        .andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(
            content()
                .string(
                    new ObjectMapper()
                        .writeValueAsString(
                            new ErrorResponse(EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ))));
  }

  @Test
  public void testProviderServiceUnavailableButRetrievesFromCache() throws Exception {

    String amount = "40";

    // Request object that will be created from the query params
    final RequestBean requestBean = new RequestBean();
    requestBean.setFrom(EUR);
    requestBean.setTo(GBP);
    requestBean.setAmount(amount);

    // Shutdown backend provider service before call, wait for 4 seconds (caching is for 5 seconds)
    this.mockRestServer.shutdown();
    TimeUnit.SECONDS.sleep(4);

    SuccessResponse expectedResponse =
        new SuccessResponse(
            requestBean,
            requestBean.getAmountNumeric().multiply(EUR_GBP_RATE_NUMERIC),
            EUR_GBP_RATE_NUMERIC);

    this.mockMvc
        .perform(get("/convert?from=" + EUR + "&to=" + GBP + "&amount=" + amount))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(new ObjectMapper().writeValueAsString(expectedResponse)));
  }

  @Test
  public void testUnderEmptyQueryParams() throws Exception {

    String amount = "40";

    // Request object that will be created from the query params
    final RequestBean requestBean = new RequestBean();
    requestBean.setFrom("");
    requestBean.setTo("");
    requestBean.setAmount(amount);

    // Should not make the request from the provider, validator should fail
    this.mockMvc
        .perform(get("/convert?from=&to=&amount=" + amount))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(
            content()
                .string(
                    org.hamcrest.Matchers.containsString("from: " + MSG_FIELD_SHOULD_BE_PROVIDED)))
        .andExpect(
            content()
                .string(
                    org.hamcrest.Matchers.containsString("to: " + MSG_FIELD_SHOULD_BE_PROVIDED)));
  }

  @Test
  public void testRequestedCurrencyNotAvailable() throws Exception {

    String amount = "40";

    // Request object that will be created from the query params
    final RequestBean requestBean = new RequestBean();
    requestBean.setFrom(EUR);
    requestBean.setTo(TRY);
    requestBean.setAmount(amount);

    // Should not make the request from the provider, validator should fail
    this.mockMvc
        .perform(get("/convert?from=" + EUR + "&to=" + TRY + "&amount=" + amount))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(
            content()
                .string(
                    org.hamcrest.Matchers.containsString(
                        "to: " + MSG_THIS_CURRENCY_IS_NOT_AVAILABLE)));
  }

  @Test
  public void testAmountNotNumeric() throws Exception {

    String amount = "A4B0";

    // Request object that will be created from the query params
    final RequestBean requestBean = new RequestBean();
    requestBean.setFrom(EUR);
    requestBean.setTo(GBP);
    requestBean.setAmount(amount);

    // Should not make the request from the provider, validator should fail
    this.mockMvc
        .perform(get("/convert?from=" + EUR + "&to=" + GBP + "&amount=" + amount))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(
            content()
                .string(
                    org.hamcrest.Matchers.containsString(
                        "amount: " + AMOUNT_FIELD_SHOULD_BE_NUMERIC)));
  }
}
