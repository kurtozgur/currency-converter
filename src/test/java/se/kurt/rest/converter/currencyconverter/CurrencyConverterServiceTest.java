package se.kurt.rest.converter.currencyconverter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static se.kurt.rest.converter.MessageConstants.EXCHANGE_RATE_SERVER_ERROR;
import static se.kurt.rest.converter.MessageConstants.EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ;
import static se.kurt.rest.converter.MessageConstants.NO_CONVERSION_RATE_WAS_FOUND_FOR_THE_GIVEN_TARGET_CURRENCY;
import static se.kurt.rest.converter.VerificationUtils.EUR;
import static se.kurt.rest.converter.VerificationUtils.SEK;
import static se.kurt.rest.converter.VerificationUtils.verifySuccessResponse;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.kurt.rest.converter.VerificationUtils;
import se.kurt.rest.converter.exceptions.ConversionFailedException;
import se.kurt.rest.converter.exceptions.UnknownResponseException;
import se.kurt.rest.converter.model.RequestBean;
import se.kurt.rest.converter.model.SuccessResponse;
import se.kurt.rest.converter.providers.ExchangeRateProvider;
import se.kurt.rest.converter.providers.exchangeratesio.LatestRateResponseBean;

class CurrencyConverterServiceTest {

  private static final BigDecimal EUR_SEK_RATE = new BigDecimal(VerificationUtils.EUR_SEK_RATE);
  private final RequestBean requestBean = new RequestBean();

  // Mocks
  private ExchangeRateProvider provider;
  private LatestRateResponseBean latestRateResponseBean;

  @BeforeEach
  void setUp() {
    this.provider = mock(ExchangeRateProvider.class);
    this.latestRateResponseBean = mock(LatestRateResponseBean.class);

    // Prepare common request
    this.requestBean.setFrom(EUR);
    this.requestBean.setTo(SEK);
    this.requestBean.setAmount("100");
  }

  @Test
  void testCorrectRateRetrievedWhenReturnedValueIsValidDecimal() {
    doReturn(Optional.of(this.latestRateResponseBean))
        .when(this.provider)
        .getLatestRate(this.requestBean);

    Map<String, BigDecimal> returnedRates = new HashMap<>();
    returnedRates.put(requestBean.getTo(), EUR_SEK_RATE);

    when(this.latestRateResponseBean.isSuccess()).thenReturn(true);
    when(this.latestRateResponseBean.getRates()).thenReturn(returnedRates);
    when(this.latestRateResponseBean.getLatestRateFor(this.requestBean.getTo()))
        .thenReturn(EUR_SEK_RATE);

    SuccessResponse expectedResponse =
        new SuccessResponse(
            this.requestBean,
            this.requestBean.getAmountNumeric().multiply(EUR_SEK_RATE),
            EUR_SEK_RATE);

    SuccessResponse actualResponse =
        CurrencyConverterService.wrapResultInRestResponse(this.provider, this.requestBean);

    verifySuccessResponse(expectedResponse, actualResponse);
  }

  @Test
  void testConvertCorrectlyWhenReturnedValueIsValidDecimal() {
    doReturn(Optional.of(this.latestRateResponseBean))
        .when(this.provider)
        .getLatestRate(this.requestBean);

    Map<String, BigDecimal> returnedRates = new HashMap<>();
    returnedRates.put(requestBean.getTo(), EUR_SEK_RATE);

    when(this.latestRateResponseBean.isSuccess()).thenReturn(true);
    when(this.latestRateResponseBean.getRates()).thenReturn(returnedRates);
    when(this.latestRateResponseBean.getLatestRateFor(this.requestBean.getTo()))
        .thenReturn(EUR_SEK_RATE);

    BigDecimal actualResponse =
        CurrencyConverterService.getTargetRateFromProvider(this.provider, this.requestBean);

    assertEquals(EUR_SEK_RATE, actualResponse);
  }

  @Test
  void testConvertThrowsExceptionWhenProviderReturnsEmpty() {
    doReturn(Optional.empty()).when(this.provider).getLatestRate(this.requestBean);

    Throwable thrown =
        assertThrows(
            UnknownResponseException.class,
            () ->
                CurrencyConverterService.getTargetRateFromProvider(
                    this.provider, this.requestBean));

    assertTrue(thrown.getMessage().equals(EXCHANGE_SERVER_RESPONSE_CAN_NOT_BE_READ));
  }

  @Test
  void testConvertThrowsExceptionWhenResponseIsUnsuccessful() {
    doReturn(Optional.of(this.latestRateResponseBean))
        .when(this.provider)
        .getLatestRate(this.requestBean);

    when(this.latestRateResponseBean.isSuccess()).thenReturn(false);
    when(this.latestRateResponseBean.getLatestRateFor(this.requestBean.getTo()))
        .thenReturn(EUR_SEK_RATE);

    Throwable thrown =
        assertThrows(
            ConversionFailedException.class,
            () ->
                CurrencyConverterService.getTargetRateFromProvider(
                    this.provider, this.requestBean));

    assertTrue(thrown.getMessage().equals(EXCHANGE_RATE_SERVER_ERROR));
  }

  @Test
  void testConvertThrowsExceptionWhenRateIsNotAvailable() {
    doReturn(Optional.of(this.latestRateResponseBean))
        .when(this.provider)
        .getLatestRate(this.requestBean);

    when(this.latestRateResponseBean.isSuccess()).thenReturn(true);
    when(this.latestRateResponseBean.getLatestRateFor(this.requestBean.getTo())).thenReturn(null);

    Throwable thrown =
        assertThrows(
            ConversionFailedException.class,
            () ->
                CurrencyConverterService.getTargetRateFromProvider(
                    this.provider, this.requestBean));

    assertTrue(
        thrown.getMessage().equals(NO_CONVERSION_RATE_WAS_FOUND_FOR_THE_GIVEN_TARGET_CURRENCY));
  }
}
