package se.kurt.rest.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class UtilTest {

  @Test
  void testBigDecimalToStringConversionsAreCorrect() {

    assertEquals("0.000000", Util.bigDecimalToString(new BigDecimal("0")));
    assertEquals("0.000000", Util.bigDecimalToString(new BigDecimal("0.000000123")));
    assertEquals("1.234568", Util.bigDecimalToString(new BigDecimal("1.23456790098")));
    assertEquals("1.234567", Util.bigDecimalToString(new BigDecimal("1.2345674")));
    assertEquals("0.000001", Util.bigDecimalToString(new BigDecimal("0.0000006")));

    assertNotEquals("1.234567", Util.bigDecimalToString(new BigDecimal("1.2345675")));
  }
}
