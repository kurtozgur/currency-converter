package se.kurt.rest.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;
import se.kurt.rest.converter.model.CurrencySymbol;
import se.kurt.rest.converter.model.ErrorResponse;
import se.kurt.rest.converter.model.SuccessResponse;

public class VerificationUtils {

  public static final String EUR = "EUR";
  public static final String SEK = "SEK";
  public static final String GBP = "GBP";
  public static final String TRY = "TRY";
  public static final String EUR_GBP_RATE = "0.854162";
  public static final String EUR_SEK_RATE = "10.26";

  public static void verifySuccessResponse(SuccessResponse expected, SuccessResponse actual) {
    // Request
    assertEquals(expected.getSourceCurrency(), actual.getSourceCurrency());
    assertEquals(expected.getTargetCurrency(), actual.getTargetCurrency());
    assertEquals(expected.getAmount(), actual.getAmount());

    // Result
    assertEquals(expected.getConvertedAmount(), actual.getConvertedAmount());
  }

  public static void verifyErrorResponse(ErrorResponse expected, ErrorResponse actual) {
    assertEquals(expected.isSuccess(), actual.isSuccess());
    assertEquals(expected.getMessage(), actual.getMessage());
  }

  public static Set<CurrencySymbol> getValidCurrencySetSample() {
    Set<CurrencySymbol> expectedCurrencies = new HashSet<>(4);

    expectedCurrencies.add(new CurrencySymbol("ABC", "New Money"));
    expectedCurrencies.add(new CurrencySymbol("GBP", "British Money"));
    expectedCurrencies.add(new CurrencySymbol("JPY", "Japanese Money"));
    expectedCurrencies.add(new CurrencySymbol("EUR", "European Money"));

    return expectedCurrencies;
  }
}
